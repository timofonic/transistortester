\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Eigenschaften}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Hardware}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Die Schaltung des TransistorTesters}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Erweiterungen f\IeC {\"u}r den TransistorTester}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Schutz der ATmega-Eing\IeC {\"a}nge}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Zenerspannungsmessung}{11}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Frequenzgenerator}{12}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Frequenzz\IeC {\"a}hler}{12}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Impulsdrehgeber}{13}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Anschluss eines graphischen Displays}{15}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Hinweise f\IeC {\"u}r den Aufbau des TransistorTesters}{19}{section.2.3}
\contentsline {section}{\numberline {2.4}Umr\IeC {\"u}stung von Tester Versionen nach Markus F.}{20}{section.2.4}
\contentsline {section}{\numberline {2.5}Chinesische Nachbauten mit Textdisplay}{21}{section.2.5}
\contentsline {section}{\numberline {2.6}Chinesische Nachbauten mit graphischem Display}{22}{section.2.6}
\contentsline {section}{\numberline {2.7}Erweiterte Schaltung mit ATmega644 oder ATmega1284}{27}{section.2.7}
\contentsline {section}{\numberline {2.8}Aufbau mit ATmega1280 oder Arduino Mega}{29}{section.2.8}
\contentsline {section}{\numberline {2.9}Programmierung des Mikrocontrollers}{31}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Benutzung der Makefile unter Linux}{31}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Benutzung des WinAVR-Paketes unter Windows}{32}{subsection.2.9.2}
\contentsline {section}{\numberline {2.10}Fehlersuche}{34}{section.2.10}
\contentsline {chapter}{\numberline {3}Bedienungshinweise}{36}{chapter.3}
\contentsline {section}{\numberline {3.1}Der Messbetrieb}{36}{section.3.1}
\contentsline {section}{\numberline {3.2}Optionale Men\IeC {\"u}funktionen f\IeC {\"u}r den ATmega328}{37}{section.3.2}
\contentsline {section}{\numberline {3.3}Selbsttest und Kalibration}{40}{section.3.3}
\contentsline {section}{\numberline {3.4}Besondere Benutzungshinweise}{40}{section.3.4}
\contentsline {section}{\numberline {3.5}Problemf\IeC {\"a}lle}{41}{section.3.5}
\contentsline {section}{\numberline {3.6}Messung von PNP- und NPN-Transistoren}{41}{section.3.6}
\contentsline {section}{\numberline {3.7}Messung von JFET- und D-MOS-Transistoren}{42}{section.3.7}
\contentsline {section}{\numberline {3.8}Messung von E-MOS Transistoren}{42}{section.3.8}
\contentsline {chapter}{\numberline {4}Konfigurieren des TransistorTesters}{43}{chapter.4}
\contentsline {chapter}{\numberline {5}Beschreibung des Messverfahrens}{53}{chapter.5}
\contentsline {section}{\numberline {5.1}Messung von Halbleitern}{55}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Messung eines PNP-Transistors oder eines P-Kanal MOSFETs}{56}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Messung eines NPN-Transistors oder eines N-Kanal-MOSFET}{57}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Vereinfachter Ablauf der Transistorerkennung}{60}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Messung von Dioden}{62}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Ergebnisse der verschiedenen Messungen}{63}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Widerstands-Messung}{67}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Widerstandsmessung mit den 680-Ohm-Widerst\IeC {\"a}nden}{67}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Widerstandsmessung mit den 470-kOhm-Widerst\IeC {\"a}nden}{69}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Ergebnisse der Widerstands-Messung}{70}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Messen von Kondensatoren}{75}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Entladen der Kondensatoren}{75}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Messung von gro\IeC {\ss }en Kapazit\IeC {\"a}ten}{75}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Messen von kleinen Kapazit\IeC {\"a}ten}{78}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Messen des \IeC {\"a}quivalenten Serienwiderstandes ESR}{80}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Messen des \IeC {\"a}quivalenten Serienwiderstandes ESR, Methode 1}{83}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Messen des \IeC {\"a}quivalenten Serienwiderstandes ESR, Methode 2}{87}{subsection.5.3.6}
\contentsline {subsection}{\numberline {5.3.7}Spannungsverlust nach einem Ladepuls, Vloss}{92}{subsection.5.3.7}
\contentsline {subsection}{\numberline {5.3.8}Separate Kapazit\IeC {\"a}ts- und ESR-Messung}{93}{subsection.5.3.8}
\contentsline {subsection}{\numberline {5.3.9}Ergebnisse der Kondensator-Messung}{94}{subsection.5.3.9}
\contentsline {subsection}{\numberline {5.3.10}Automatischer Abgleich der Kondensator-Messung}{98}{subsection.5.3.10}
\contentsline {section}{\numberline {5.4}Messen von Induktivit\IeC {\"a}ten}{102}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Ergebnisse der Induktivit\IeC {\"a}ts-Messungen}{103}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Selbsttest-Funktion}{104}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Einige Selbsttest-Ergebnisse}{108}{subsection.5.5.1}
\contentsline {section}{\numberline {5.6}Frequenzmessung}{111}{section.5.6}
\contentsline {chapter}{\numberline {6}Signal-Erzeugung}{113}{chapter.6}
\contentsline {section}{\numberline {6.1}Frequenz-Erzeugung}{113}{section.6.1}
\contentsline {section}{\numberline {6.2}Pulsweiten-Erzeugung}{113}{section.6.2}
\contentsline {chapter}{\numberline {7}Bekannte Fehler und ungel\IeC {\"o}ste Probleme}{114}{chapter.7}
\contentsline {chapter}{\numberline {8}Spezielle Softwareteile}{116}{chapter.8}
\contentsline {chapter}{\numberline {9}Arbeitsliste und neue Ideen}{117}{chapter.9}
