\select@language {english}
\contentsline {chapter}{\numberline {1}Features}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Hardware}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Circuit of the TransistorTester}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Extensions for the TransistorTester}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Protection of the ATmega inputs}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Measurement of zener voltage above 4 Volt}{11}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Frequency generator}{12}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Frequency measurement}{12}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Using of a rotary pulse encoder}{13}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Connection of a graphical display}{15}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Hints for building the TransistorTester}{19}{section.2.3}
\contentsline {section}{\numberline {2.4}Changeover for tester versions designed by Markus F.}{19}{section.2.4}
\contentsline {section}{\numberline {2.5}Chinese clones with text display}{21}{section.2.5}
\contentsline {section}{\numberline {2.6}Chinese clones with graphical display}{22}{section.2.6}
\contentsline {section}{\numberline {2.7}Extented circuit with ATmega644 or ATmega1284}{26}{section.2.7}
\contentsline {section}{\numberline {2.8}Buildup of a tester with ATmega1280 or Arduino Mega}{28}{section.2.8}
\contentsline {section}{\numberline {2.9}Programming of the microcontroller}{30}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Using the Makefile with Linux}{30}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Using the WinAVR package with Windows}{31}{subsection.2.9.2}
\contentsline {section}{\numberline {2.10}Troubleshooting}{33}{section.2.10}
\contentsline {chapter}{\numberline {3}Instructions for use}{35}{chapter.3}
\contentsline {section}{\numberline {3.1}The measurement operation}{35}{section.3.1}
\contentsline {section}{\numberline {3.2}Optional menu functions for the ATmega328}{36}{section.3.2}
\contentsline {section}{\numberline {3.3}Selftest and Calibration}{38}{section.3.3}
\contentsline {section}{\numberline {3.4}special using hints}{39}{section.3.4}
\contentsline {section}{\numberline {3.5}Compoments with problems}{40}{section.3.5}
\contentsline {section}{\numberline {3.6}Measurement of PNP and NPN transistors}{40}{section.3.6}
\contentsline {section}{\numberline {3.7}Measurement of JFET and D-MOS transistors}{41}{section.3.7}
\contentsline {section}{\numberline {3.8}Measurement of E-MOS transistors}{41}{section.3.8}
\contentsline {chapter}{\numberline {4}Configuring the TransistorTester}{42}{chapter.4}
\contentsline {chapter}{\numberline {5}Description of the measurement procedures}{52}{chapter.5}
\contentsline {section}{\numberline {5.1}Measurement of Semiconductors}{54}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Measurement of PNP Transistor or P-Channel-MOSFET}{55}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Measurement of NPN Transistor or N-Channel-MOSFET}{57}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Simplified flowchart of the transistors tests}{60}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Measurement of Diodes}{62}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Results of different measurements}{63}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Resistor Measurement}{67}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Resistor Measurement with 680 Ohm Resistors}{67}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Resistor Measurement with 470 kOhm resistors}{69}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Results of the resistor measurements}{70}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Measurement of Capacitors}{75}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Discharging of Capacitors}{75}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Measurement of big Capacitors}{75}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Measurement of small Capacitors}{78}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Measurement of the Equivalent Series Resistance ESR}{80}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Measurement of the Equivalent Series Resistance ESR, first way}{82}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Measurement of the Equivalent Series Resistance ESR, second way}{85}{subsection.5.3.6}
\contentsline {subsection}{\numberline {5.3.7}Voltage loss after a load pulse, Vloss}{91}{subsection.5.3.7}
\contentsline {subsection}{\numberline {5.3.8}Separate capacity and ESR measurement}{92}{subsection.5.3.8}
\contentsline {subsection}{\numberline {5.3.9}Results of Capacitor measurement}{93}{subsection.5.3.9}
\contentsline {subsection}{\numberline {5.3.10}Automatic calibration of the capacitor measurement}{97}{subsection.5.3.10}
\contentsline {section}{\numberline {5.4}Measurement of inductance}{101}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Results of the inductance measurements}{102}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Selftest Function}{103}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Some Results of the Selftest Function}{107}{subsection.5.5.1}
\contentsline {section}{\numberline {5.6}Measurement of frequency}{110}{section.5.6}
\contentsline {chapter}{\numberline {6}Signal generation}{112}{chapter.6}
\contentsline {section}{\numberline {6.1}Frequency Generation}{112}{section.6.1}
\contentsline {section}{\numberline {6.2}Puls width generation}{112}{section.6.2}
\contentsline {chapter}{\numberline {7}Known errors and unsolved problems}{113}{chapter.7}
\contentsline {chapter}{\numberline {8}Special Software Parts}{114}{chapter.8}
\contentsline {chapter}{\numberline {9}To Do List and new ideas}{115}{chapter.9}
